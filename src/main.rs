mod sense_hat;

use log::{info, error};
use i2cdev::core::*;
use i2cdev::linux::{LinuxI2CError, LinuxI2CDevice, LinuxI2CMessage};

const HTS221_ADDR: u16 = 0x5f;

fn main() {
    env_logger::init();
    info!("starting up");
    sense_hat::humidity_hts221::HumidityHTS221::init();

    match LinuxI2CDevice::new("/dev/i2c-1", HTS221_ADDR) {
        Ok(mut humidity) => {
            info!("Humidity device created");
            let mut read_data = [0; 1];
            let mut msgs = [
                LinuxI2CMessage::write(&[0x0f]),
                LinuxI2CMessage::read(&mut read_data)
            ];
            humidity.transfer(&mut msgs).unwrap();
            info!("Identifier: 0x{:02x}", read_data[0]);
        },
        Err(..) => error!("No humidity device found!"),
    }
}
